package com.rahasak.letsencrypt

import com.rahasak.letsencrypt.crypto.CryptoFactory

object Main extends App {

  CryptoFactory.init()

  val idRsa = CryptoFactory.getIdRsaStr()
  println(idRsa)

  val idRsaPub = CryptoFactory.getIdRsaPubStr()
  println(idRsaPub)

  val sig = CryptoFactory.sign("rahasaklabs")
  println(sig)

  val verified = CryptoFactory.verify("rahasaklabs", sig)
  println(verified)

  val cipherText = CryptoFactory.encryptRsa("rahasaklabs")
  println(cipherText)

  val plainTex = CryptoFactory.decryptRsa(cipherText)
  println(plainTex)

}
