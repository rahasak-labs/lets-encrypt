package com.rahasak.letsencrypt.crypto

import java.io.{File, FileInputStream, FileOutputStream}
import java.security._
import java.security.spec.{PKCS8EncodedKeySpec, X509EncodedKeySpec}

import com.rahasak.letsencrypt.config.Config
import javax.crypto.Cipher
import sun.misc.{BASE64Decoder, BASE64Encoder}

object CryptoFactory extends Config {

  def init(): Unit = {
    // first create .keys directory
    val dir: File = new File(keysDir)
    if (!dir.exists) {
      dir.mkdir
    }

    // generate keys if not exists
    val filePublicKey = new File(publicKeyLocation)
    if (!filePublicKey.exists) {
      initKeyPair()
    }
  }

  def initKeyPair(): Unit = {
    // generate key pair
    val keyPairGenerator = KeyPairGenerator.getInstance("RSA")
    keyPairGenerator.initialize(keySize, new SecureRandom)
    val keyPair: KeyPair = keyPairGenerator.generateKeyPair

    // save public key
    val x509keySpec = new X509EncodedKeySpec(keyPair.getPublic.getEncoded)
    val publicKeyStream = new FileOutputStream(publicKeyLocation)
    publicKeyStream.write(x509keySpec.getEncoded)

    // save private key
    val pkcs8KeySpec = new PKCS8EncodedKeySpec(keyPair.getPrivate.getEncoded)
    val privateKeyStream = new FileOutputStream(privateKeyLocation)
    privateKeyStream.write(pkcs8KeySpec.getEncoded)
  }

  def getKeyPair(): KeyPair = {
    // read public key
    val filePublicKey = new File(publicKeyLocation)
    var inputStream = new FileInputStream(publicKeyLocation)
    val encodedPublicKey: Array[Byte] = new Array[Byte](filePublicKey.length.toInt)
    inputStream.read(encodedPublicKey)
    inputStream.close()

    // read private key
    val filePrivateKey = new File(privateKeyLocation)
    inputStream = new FileInputStream(privateKeyLocation)
    val encodedPrivateKey: Array[Byte] = new Array[Byte](filePrivateKey.length.toInt)
    inputStream.read(encodedPrivateKey)
    inputStream.close()

    val keyFactory: KeyFactory = KeyFactory.getInstance("RSA")

    // public key
    val publicKeySpec: X509EncodedKeySpec = new X509EncodedKeySpec(encodedPublicKey)
    val publicKey: PublicKey = keyFactory.generatePublic(publicKeySpec)

    // private key
    val privateKeySpec: PKCS8EncodedKeySpec = new PKCS8EncodedKeySpec(encodedPrivateKey)
    val privateKey: PrivateKey = keyFactory.generatePrivate(privateKeySpec)

    new KeyPair(publicKey, privateKey)
  }

  def getIdRsaStr(): String = {
    // get private key via key pair
    val keyPair = getKeyPair()
    val privateKeyStream = keyPair.getPrivate.getEncoded

    // BASE64 encoded string
    new BASE64Encoder().encode(privateKeyStream).replaceAll("\n", "").replaceAll("\r", "")
  }

  def getIdRsaPubStr(): String = {
    // get public key via key pair
    val keyPair = getKeyPair()
    val publicKeyStream = keyPair.getPublic.getEncoded

    // BASE64 encoded string
    new BASE64Encoder().encode(publicKeyStream).replaceAll("\n", "").replaceAll("\r", "")
  }

  def getIdRsaFromStr(keyStr: String): PrivateKey = {
    val spec = new X509EncodedKeySpec(new BASE64Decoder().decodeBuffer(keyStr))
    val kf = KeyFactory.getInstance("RSA")
    kf.generatePrivate(spec)
  }

  def getIdRsaPubFromStr(keyStr: String): PublicKey = {
    val spec = new X509EncodedKeySpec(new BASE64Decoder().decodeBuffer(keyStr))
    val kf = KeyFactory.getInstance("RSA")
    kf.generatePublic(spec)
  }

  def sign(payload: String) = {
    // get private key via key pair
    val privateKey = getKeyPair().getPrivate

    // sign the payload
    val signature = Signature.getInstance("SHA256withRSA")
    signature.initSign(privateKey)
    signature.update(payload.getBytes)

    // signature as Base64 encoded string
    new BASE64Encoder().encode(signature.sign).replaceAll("\n", "").replaceAll("\r", "")
  }

  def verify(payload: String, signedPayload: String): Boolean = {
    // get public key via key pair
    val publicKey = getKeyPair().getPublic

    val signature = Signature.getInstance("SHA256withRSA")
    signature.initVerify(publicKey)
    signature.update(payload.getBytes)

    // decode(BASE64) signed payload and verify signature
    signature.verify(new BASE64Decoder().decodeBuffer(signedPayload))
  }

  def encryptRsa(payload: String): String = {
    // get public key via key pair
    val publicKey = getKeyPair().getPublic

    val cipher: Cipher = Cipher.getInstance("RSA")
    cipher.init(Cipher.ENCRYPT_MODE, publicKey)
    val data = cipher.doFinal(payload.getBytes)

    // encode base64
    new BASE64Encoder().encode(data).replaceAll("\n", "").replaceAll("\r", "")
  }

  def decryptRsa(payload: String): String = {
    // get private key via key pair
    val privateKey = getKeyPair().getPrivate

    // payload comes with base64 string
    val data = new BASE64Decoder().decodeBuffer(payload)

    val cipher: Cipher = Cipher.getInstance("RSA")
    cipher.init(Cipher.DECRYPT_MODE, privateKey)
    new String(cipher.doFinal(data))
  }

  def hashSha256(payload: String): String = {
    val digest = MessageDigest.getInstance("SHA-256")
    val hash = digest.digest(payload.getBytes)

    new BASE64Encoder().encode(hash).replaceAll("\n", "").replaceAll("\r", "")
  }

  def encodeBase64(payload: String): String = {
    new BASE64Encoder().encode(payload.getBytes())
      .replaceAll("\n", "")
      .replaceAll("\r", "")
  }

}
