package com.rahasak.letsencrypt.config

import com.typesafe.config.ConfigFactory

import scala.util.Try

trait Config {

  // config object
  val appConf = ConfigFactory.load()

  // keys config
  lazy val keysDir = Try(appConf.getString("keys.dir")).getOrElse(".keys")
  lazy val publicKeyLocation = Try(appConf.getString("keys.public-key-location")).getOrElse(".keys/id_rsa.pub")
  lazy val privateKeyLocation = Try(appConf.getString("keys.private-key-location")).getOrElse(".keys/id_rsa")
  lazy val keySize = Try(appConf.getInt("keys.size")).getOrElse(1024)

}
