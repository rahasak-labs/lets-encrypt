name := "lets-encrypt"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies ++= {

  Seq(
    "org.slf4j"             % "slf4j-api"               % "1.7.5",
    "ch.qos.logback"        % "logback-classic"         % "1.0.9",
    "com.typesafe"          % "config"                  % "1.4.0",
    "org.bouncycastle"      % "bcprov-jdk16"            % "1.45"
  )

}

resolvers ++= Seq(
  "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"
)
